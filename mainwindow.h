#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void createRGBCube();
    qreal getAngle(QPointF& first, QPointF& second);
    QVector<QImage> valueImages;


private slots:
    void valueChanged(int value);
    void sliderValueChanged(int value);
    void sliderRelased();
    void convertToRGB();
    void convertToCMYK();
};

#endif // MAINWINDOW_H
