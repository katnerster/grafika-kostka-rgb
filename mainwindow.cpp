#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QPixmap>
#include <QtMath>
#include <QDebug>
#include <QImage>
#include <QTime>
#include <QMessageBox>
#include <QVector>
#include <algorithm>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    for (int value = 0; value <256; value++)
    {
        QPixmap l(256,256);
        QPainter pp(&l);
        for (int x = 0; x < value; x++)
        {
            QColor *color = new QColor();
            color->setHsv(0, 255,255);
            QLinearGradient g(0,x,255,x);
            for (int i = 0; i < 6; i++)
            {
                color = new QColor();
                color->setHsvF((i/6.0), (float)x/255,(float)value/255);
                g.setColorAt(i/6.0, *color);
            }

            color = new QColor();
            color->setHsvF((0/6.0), (float)x/255,(float)value/255);
            g.setColorAt(6/6.0, *color);
            pp.fillRect(0,x,255,x, g);

        }
        QImage image = l.toImage();
        valueImages.append(image);
    }


    createRGBCube();
        valueChanged(255);
}

MainWindow::~MainWindow()
{
    delete ui;
}

qreal MainWindow::getAngle(QPointF&first, QPointF& second)
{
    return qRadiansToDegrees(atan2(second.y() - first.y(), second.x() - first.x()));
}

void MainWindow::valueChanged(int value)
{
    qDebug()<< value;
    QTime myTimer;
    myTimer.start();
    QImage image = valueImages.at(value);

    QImage p(512,512, QImage::Format_RGB32);
    p.fill(QColor(Qt::black));
    QPainter painter(&p);
    for (int r = value; r>0;r--)
    {
        for (float theta = 0; theta < M_PI*2; theta += 0.001)
        {
            QColor cc = image.pixel((int)(theta/(M_PI*2)*255), r);
            float x = r* qCos(theta);
            float y = r * qSin(theta);
            p.setPixel(x+255,y+255, cc.rgb());
        }
    }

    qDebug() << myTimer.elapsed();
    ui->label->setPixmap(QPixmap::fromImage(p));
}

void MainWindow::sliderValueChanged(int value)
{
    ui->spinBox->blockSignals(true);
    ui->spinBox->setValue(value);
    ui->spinBox->blockSignals(false);
}

void MainWindow::sliderRelased()
{
    valueChanged(ui->verticalSlider->value());
}

void MainWindow::convertToRGB()
{
    qDebug() << "convertToRGB();";
    bool ok;
    float c = ui->cLineEdit->text().toFloat(&ok) / 100;
    if (!ok || c < 0 || c > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość C(Powinno być 0-100)");
        errorBox.exec();
        return;
    }

    float m = ui->mLineEdit->text().toFloat(&ok) / 100;
    if (!ok || m < 0 || m > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość M(Powinno być 0-100)");
        errorBox.exec();
        return;
    }

    float y = ui->yLineEdit->text().toFloat(&ok) / 100;
    if (!ok || y < 0 || y > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość Y(Powinno być 0-100)");
        errorBox.exec();
        return;
    }

    float k = ui->kLineEdit->text().toFloat(&ok) / 100;
    if (!ok || k < 0 || k > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość K(Powinno być 0-100)");
        errorBox.exec();
        return;
    }

    float red = 1- std::min({1.0f, (c*(1-k)+k)});
    float green = 1 - std::min({1.0f, (m*(1-k))+k});
    float blue = 1 - std::min({1.0f, (y*(1-k))+k});

    ui->rLineEdit->setText(QString::number((int)(red*255)));

    ui->gLineEdit->setText(QString::number((int)(green*255)));

    ui->bLineEdit->setText(QString::number((int)(blue*255)));
    QImage colorImage(50,50, QImage::Format_RGB32);
    colorImage.fill(QColor((int)(red*255),(int)(green*255),(int)(blue*255)));
    ui->label_3->setPixmap(QPixmap::fromImage(colorImage));
}

void MainWindow::convertToCMYK()
{
    qDebug() << "convertToCMYK();";
    bool ok;
    float r = ui->rLineEdit->text().toFloat(&ok) / 255;
    if (!ok || r < 0 || r > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość R(Powinno być 0-255");
        errorBox.exec();
        return;
    }
float g = ui->gLineEdit->text().toFloat(&ok) / 255;
    if (!ok || g < 0 || g > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość G(Powinno być 0-255");
        errorBox.exec();
        return;
    }
float b = ui->bLineEdit->text().toFloat(&ok) / 255;
    if (!ok || b < 0 || b > 1.0)
    {
        QMessageBox errorBox;
        errorBox.setText("Wprowadzono niepoprawną wartość B(Powinno być 0-255");
        errorBox.exec();
        return;
    }
    float black = std::min({1-r, 1-g, 1-b});
    float cyan = (1-r-black)/(1-black);
    float magenta = (1-g-black)/(1-black);
    float yellow = (1-b-black)/(1-black);

    ui->kLineEdit->setText(QString::number((int)(black*100)));

    ui->cLineEdit->setText(QString::number((int)(cyan*100)));

    ui->mLineEdit->setText(QString::number((int)(magenta*100)));

    ui->yLineEdit->setText(QString::number((int)(yellow*100)));
    QImage colorImage(50,50, QImage::Format_RGB32);
    colorImage.fill(QColor((int)(r*255),(int)(g*255),(int)(b*255)));
    ui->label_3->setPixmap(QPixmap::fromImage(colorImage));
}

void MainWindow::createRGBCube()
{
    QPixmap gora(256,256);
    QPainter paint(&gora);
    for (int i = 0; i <256; i++)
    {
        for (int j = 0; j < 256; j++)
        {
            QPen pen = paint.pen();
            pen.setColor(QColor(j,255,i));
            paint.setPen(pen);
            paint.drawPoint(QPoint(i,255-j));
        }
    }

    QPixmap przod(256,256);
    QPainter paint2(&przod);
    for (int i = 0; i <256; i++)
    {
        for (int j = 0; j < 256; j++)
        {
            QPen pen = paint.pen();
            pen.setColor(QColor(0,255-j,i));
            paint2.setPen(pen);
            paint2.drawPoint(QPoint(i,j));
        }
    }

    QPixmap bok(256,256);
    QPainter paint3(&bok);
    for (int i = 0; i <256; i++)
    {
        for (int j = 0; j < 256; j++)
        {
            QPen pen = paint.pen();
            pen.setColor(QColor(i,255-j,255));
            paint3.setPen(pen);
            paint3.drawPoint(QPoint(i,j));
        }
    }
    ui->setupUi(this);
    QPixmap pixmap(500,500);
    pixmap.fill(Qt::black);
    QPainter painter(&pixmap);

    QPolygonF p1;
    p1 << QPointF(100,100) << QPointF(100,355) << QPointF(355,355) <<QPointF(355,100);
    QPolygonF p;
    p << QPointF(100,100) << QPointF(50,150) << QPointF(150,150) <<QPointF(200,100);
    QTransform t;
    t.quadToQuad(p1, p,t);
    painter.save();
    painter.setTransform(t);
    painter.drawRect(QRect(QPoint(100,100), QPoint(200,200)));
    painter.drawImage(100,100, gora.toImage());
    painter.restore();
    p.clear();
    p << QPointF(50,150) << QPointF(50,250) << QPointF(150,250) <<QPointF(150,150);
    t.quadToQuad(p1, p,t);
    painter.save();
    painter.setTransform(t);
    painter.drawImage(100,100, przod.toImage());

    p.clear();
    p << QPointF(150,150) << QPointF(150,250) << QPointF(200,200) <<QPointF(200,100);
    t.quadToQuad(p1, p,t);
    painter.save();
    painter.setTransform(t);
    painter.drawImage(100,100, bok.toImage());
    ui->label_2->setPixmap(pixmap);
}
