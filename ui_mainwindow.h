/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QSlider *verticalSlider;
    QSpinBox *spinBox;
    QWidget *tab_2;
    QGridLayout *gridLayout_3;
    QLabel *label_2;
    QWidget *widget;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLabel *label_3;
    QFormLayout *formLayout;
    QLabel *rLabel;
    QLineEdit *rLineEdit;
    QLabel *gLabel;
    QLineEdit *gLineEdit;
    QLabel *bLabel;
    QLineEdit *bLineEdit;
    QFormLayout *formLayout_2;
    QLabel *cLabel;
    QLineEdit *cLineEdit;
    QLabel *mLabel;
    QLineEdit *mLineEdit;
    QLabel *yLabel;
    QLineEdit *yLineEdit;
    QLabel *kLabel;
    QLineEdit *kLineEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(694, 381);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setTabShape(QTabWidget::Triangular);
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        gridLayout_2 = new QGridLayout(tab);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label = new QLabel(tab);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 2, 2);

        verticalSlider = new QSlider(tab);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setMaximum(255);
        verticalSlider->setValue(255);
        verticalSlider->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(verticalSlider, 1, 1, 1, 1);

        spinBox = new QSpinBox(tab);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMaximum(255);
        spinBox->setValue(255);

        gridLayout_2->addWidget(spinBox, 2, 1, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        gridLayout_3 = new QGridLayout(tab_2);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_2 = new QLabel(tab_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        widget = new QWidget();
        widget->setObjectName(QStringLiteral("widget"));
        gridLayout_4 = new QGridLayout(widget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        pushButton = new QPushButton(widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_2 = new QPushButton(widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);


        gridLayout_4->addLayout(verticalLayout, 0, 1, 1, 1);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        rLabel = new QLabel(widget);
        rLabel->setObjectName(QStringLiteral("rLabel"));

        formLayout->setWidget(0, QFormLayout::LabelRole, rLabel);

        rLineEdit = new QLineEdit(widget);
        rLineEdit->setObjectName(QStringLiteral("rLineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, rLineEdit);

        gLabel = new QLabel(widget);
        gLabel->setObjectName(QStringLiteral("gLabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, gLabel);

        gLineEdit = new QLineEdit(widget);
        gLineEdit->setObjectName(QStringLiteral("gLineEdit"));

        formLayout->setWidget(1, QFormLayout::FieldRole, gLineEdit);

        bLabel = new QLabel(widget);
        bLabel->setObjectName(QStringLiteral("bLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, bLabel);

        bLineEdit = new QLineEdit(widget);
        bLineEdit->setObjectName(QStringLiteral("bLineEdit"));

        formLayout->setWidget(2, QFormLayout::FieldRole, bLineEdit);


        gridLayout_4->addLayout(formLayout, 0, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        cLabel = new QLabel(widget);
        cLabel->setObjectName(QStringLiteral("cLabel"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, cLabel);

        cLineEdit = new QLineEdit(widget);
        cLineEdit->setObjectName(QStringLiteral("cLineEdit"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, cLineEdit);

        mLabel = new QLabel(widget);
        mLabel->setObjectName(QStringLiteral("mLabel"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, mLabel);

        mLineEdit = new QLineEdit(widget);
        mLineEdit->setObjectName(QStringLiteral("mLineEdit"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, mLineEdit);

        yLabel = new QLabel(widget);
        yLabel->setObjectName(QStringLiteral("yLabel"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, yLabel);

        yLineEdit = new QLineEdit(widget);
        yLineEdit->setObjectName(QStringLiteral("yLineEdit"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, yLineEdit);

        kLabel = new QLabel(widget);
        kLabel->setObjectName(QStringLiteral("kLabel"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, kLabel);

        kLineEdit = new QLineEdit(widget);
        kLineEdit->setObjectName(QStringLiteral("kLineEdit"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, kLineEdit);


        gridLayout_4->addLayout(formLayout_2, 0, 2, 1, 1);

        tabWidget->addTab(widget, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 694, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);
        QObject::connect(verticalSlider, SIGNAL(valueChanged(int)), MainWindow, SLOT(sliderValueChanged(int)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), MainWindow, SLOT(valueChanged(int)));
        QObject::connect(verticalSlider, SIGNAL(sliderReleased()), MainWindow, SLOT(sliderRelased()));
        QObject::connect(pushButton, SIGNAL(clicked()), MainWindow, SLOT(convertToCMYK()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MainWindow, SLOT(convertToRGB()));

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Podstawa sto\305\274ka HSV", 0));
        label_2->setText(QApplication::translate("MainWindow", "TextLabel", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Kostka RGB", 0));
        pushButton->setText(QApplication::translate("MainWindow", "RGB -> CMYK", 0));
        pushButton_2->setText(QApplication::translate("MainWindow", "CMYK -> RGB", 0));
        label_3->setText(QString());
        rLabel->setText(QApplication::translate("MainWindow", "R", 0));
        gLabel->setText(QApplication::translate("MainWindow", "G", 0));
        bLabel->setText(QApplication::translate("MainWindow", "B", 0));
        cLabel->setText(QApplication::translate("MainWindow", "C", 0));
        mLabel->setText(QApplication::translate("MainWindow", "M", 0));
        yLabel->setText(QApplication::translate("MainWindow", "Y", 0));
        kLabel->setText(QApplication::translate("MainWindow", "K", 0));
        tabWidget->setTabText(tabWidget->indexOf(widget), QApplication::translate("MainWindow", "Konwersja RGB <-> CMYK", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
